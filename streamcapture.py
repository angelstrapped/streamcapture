import logging
import time
import os
import sys

from config import *

SLEEP_TIME = 600

# Set up logging to file and console.
log_format = "%(asctime)s %(message)s"

logging.basicConfig(
	format=log_format,
	filename='streamcapture.log',
	level=logging.DEBUG
)

stdout_logger = logging.StreamHandler(sys.stdout)
stdout_logger.setFormatter(logging.Formatter(log_format))
logging.getLogger().addHandler(stdout_logger)

logging.info("Streamcapture init.")

while True:
	logging.info("Loop.")

	# Make a new, (hopefully unique) filename.
	new_filename = TARGET_STREAM + "_" + time.strftime("%Y%m%d_%H%M%S")

	# Start streamlink.
	logging.info("Starting streamlink...")
	os.system("streamlink %s best -o %s.ts" % (TARGET_PLATFORM + TARGET_STREAM, new_filename))

	# I don't know what will happen here, in the cases of success and failure...

	# Wait 10 minutes.
	logging.info("Sleeping for %d minutes." % (SLEEP_TIME / 60))
	time.sleep(SLEEP_TIME)
